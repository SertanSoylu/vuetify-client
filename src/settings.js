const baseColor = "green";
const baseColor2 = "blue";

//export const settings = {
export default {
    appVersion: "1.0.22",
    appTitle : "SoylusofT",
    themeColor1: baseColor,
    themeColor11: baseColor + " accent-1",
    themeColor12: baseColor + " accent-2",
    themeColor13: baseColor + " accent-3",
    themeColor14: baseColor + " accent-4",
    //
    themeColor2: baseColor2,
    themeColor21: baseColor2 + " accent-1",
    themeColor22: baseColor2 + " accent-2",
    themeColor23: baseColor2 + " accent-3",
    themeColor24: baseColor2 + " accent-4",
};

