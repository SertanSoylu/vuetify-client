import Vue from "vue";
import Vuex from "vuex";
import consts from "@/controller/CommonConsts";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    messageState: {
      messageType: consts.MT_ERROR,
      messageHeader: "",
      message: "",
      messageDetail: "",
      vueInfo: "",
      showMessage: false,
    }
  },
  mutations: {
    PrepareMessageMut: function(state, payload) {
      console.log(payload);
      state.messageState.messageType = payload.messageType || consts.MT_ERROR;
      state.messageState.messageHeader = payload.messageHeader || state.messageState.messageType.defCaption || consts.MT_ERROR.defCaption;
      state.messageState.message = payload.message;
      state.messageState.messageDetail = payload.messageDetail;
      state.messageState.vueInfo = payload.vueInfo;
      state.messageState.showMessage = payload.showMessage;
    },
    ResetMessageMut: function(state) {
      state.messageState.messageType = consts.MT_ERROR,
      state.messageState.showMessage = false;
      state.messageState.messageHeader = "";
      state.messageState.message = "";
      state.messageState.messageDetail = "";
      state.messageState.vueInfo = "";
    }
  },
  actions: {
    PrepareMessage: (context, payload) => {
      context.commit("PrepareMessageMut", payload)
    },
    ResetMessage: (context) => {
      context.commit("ResetMessageMut")
    }

  }  
});
