import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
/* vuetify */
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
/* elementUI for only datetime picker */
import { DatePicker, TimePicker } from 'element-ui'
import langEL from 'element-ui/lib/locale/lang/tr-TR'
import localeEL from 'element-ui/lib/locale'
import './element-variables.scss'
/* for datetime purposes */
import moment from 'moment'
/* validation */
// import Vuelidate from 'vuelidate'
/* application globals */
import settings from './settings'
import consts from './controller/CommonConsts'
import funcs from './controller/CommonFuncs'


Vue.prototype.$moment = moment;
Vue.prototype.$settings = settings;
Vue.prototype.$consts = consts;
Vue.prototype.$funcs = funcs;
Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };


/* ie11 and safari support */
import 'babel-polyfill'

Vue.use(Vuetify)
// Vue.use(Vuelidate)

localeEL.use(langEL)
Vue.use(DatePicker, { langEL })
Vue.use(TimePicker, { langEL })


Vue.config.productionTip = false

Vue.config.errorHandler = function (err, vm, info) {
  store.dispatch('PrepareMessage', { 
    messageType: consts.MT_ERROR,
    messageHeader: "",  
    message: err.message,                                    
    messageDetail: err.stack, 
    vueInfo : info,
    showMessage: true 
  });

}

Vue.config.warnHandler = function (msg, vm, trace) {
  store.dispatch('PrepareMessage', {
    messageType: consts.MT_WARNING,
    messageHeader: "", 
    message: msg, 
    messageDetail: msg.stack,
    vueInfo: trace,
    showMessage: true 
  });
}

window.onerror = function(message, source, lineno, colno, error) {
  var s= 'source: '+source+
         ', lineno: '+lineno+
         ', colno: '+colno+
         ', error: '+error;

  store.dispatch('PrepareMessage', { 
    messageType: consts.MT_ERROR,
    messageHeader: "Window Hatası",
    message: message, 
    messageDetail: error.stack,
    vueInfo: s,
    showMessage: true,
  });
}

new Vue({
  router,
  store,
  render: function (h) { return h(App) },
}).$mount('#app')
