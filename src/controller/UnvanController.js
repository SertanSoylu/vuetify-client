import backend from "../controller/BackendController";

export default {
	class: {
		unvan_id: { 
			type: Number, 
			field: "unvan_id", 
			caption: "Ünvan ID", 
			defValue: 0, 
			required: true,
		},
		unvan_adi: { 
			type: String, 
			field: "unvan_adi", 
			caption: "Ünvanı", 
			defValue: "", 
			required: true,
		},
		aktif: { 
			type: String, 
			field: "aktif", 
			caption: "Aktif", 
			defValue: "1", 
			required: true,
		},
		tarih: { 
			type: Date, 
			field: "tarih", 
			caption: "Tarih", 
			defValue: "", 
			required: true,
		},
	},
	pkFieldName: "unvan_id",
	apiName: "unvan",
	item: {},
	dsState: 0,
	getItemObject() {
		var itemStr = `{
			"${this.class.unvan_id.field}" : ${this.class.unvan_id.defValue},
			"${this.class.unvan_adi.field}" : "${this.class.unvan_adi.defValue}",
			"${this.class.aktif.field}" : "${this.class.aktif.defValue}",
			"${this.class.tarih.field}" : "${this.class.tarih.defValue}"
		}`;	

		var res = JSON.parse(itemStr);
		return res;	
	},
	getItems() {
		return backend.axiosInstance()
		.get('/' + this.apiName)
		.then((res) => {
			if (res.data.name === "error") {
				backend.axiosCatchError(res);	
			}
			else {
				return res;
			}
		})
		.catch((err) => {
			backend.axiosCatchError(err);
		});
	},
	postItem(it) {
		return backend.axiosInstance()
		.post('/' + this.apiName, it)
		.then((res) => {
			if (res.data.name === "error") {
				backend.axiosCatchError(res);	
			}
			else {
				return res;
			}
		})
		.catch((err) => {
			console.log(err);
			backend.axiosCatchError(err);
		});
	},
	putItem(it) {
		return backend.axiosInstance()
		.put('/' + this.apiName + '/' + it.unvan_id, it)
		.then((res) => {
			if (res.data.name === "error") {
				backend.axiosCatchError(res);	
			}
			else {
				return res;
			}
		})
		.catch((err) => {
			console.log(err);
			backend.axiosCatchError(err);
		});
	},
	deleteItem(it) {
		return backend.axiosInstance()
		.delete('/' + this.apiName + '/' + it.unvan_id)
		.then((res) => {
			if (res.data.name === "error") {
				backend.axiosCatchError(res);	
			}
			else {
				return res;
			}
		})
		.catch((err) => {
			console.log(err);
			backend.axiosCatchError(err);
		});
	},	
};
