import consts from '@/controller/CommonConsts';
import moment from 'moment' // tarih islemleri icin kullaniliyor

export default {
	Date2String(tarih, showTime = true) {
		var res = moment(tarih).format(consts.dateFormat);
		if (showTime)
			res = moment(tarih).format(consts.dateTimeFormat);
		return (res);
	},
	Date2JSONString(tarih) {
		var res = moment(tarih).format();
		return (res);
	},
	String2Date(date) {
		if (!date) {
			return null
		}

		const [day, month, year] = date.split('.')
		var res = `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`
		return res
	},
	GetServerTarihiStr() {
		return this.Date2JSONString(moment());
	},
	GetBoolVal(aktif) {
		return aktif === consts.dbYes ? true : false
	}

};

