export default {
	// db constants
	dbYes: "1",
	dbNo: "0",
	// format constants
	dateFormat: "DD.MM.YYYY",
	dateTimeFormat: "DD.MM.YYYY HH:mm:ss",
	el_dateTimeFormat: "dd.MM.yyyy HH:mm:ss",
	el_json_format: "yyyy-MM-ddTHH:mm:ss.000Z",
	// messages
	msgNoRecord: "Kayıt Bulunamadı...",
	// captions
	capYeniKayit: "Yeni Kayıt",
	capGuncelle: "Güncelle",
	// message types
	MT_ERROR: {
		color: "error",
		defCaption: "Hata...",
		icon: "",
	},
	MT_SUCCESS: {
		color: "success",
		defCaption: "İşlem Tamamlandı...",
		icon: "",
	},
	MT_WARNING: {
		color: "warning",
		defCaption: "Uyarı...",
		icon: "",
	},
	MT_INFO: {
		color: "info",
		defCaption: "Bilgilendirme...",
		icon: "",
	},
	MT_COMFIRM: {
		color: "orange lighten-1",
		defCaption: "Bilgilendirme...",	
		icon: "",
	},
	// modal result
	MR_NO: 0,
	MR_YES: 1,
	// dataset state
	DS_BROWSE: 0,
	DS_INSERT: 1,
	DS_UPDATE: 2,
	DS_DELETE: 3,
};