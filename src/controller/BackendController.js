import axios from "axios";
import store from '@/store';

//const serverUrl = "http://192.168.2.23:3000";
const serverUrl = "http://localhost:3000";

export default {
  axiosInstance() {
    return axios.create({ baseURL: serverUrl });
  },
  axiosCatchError(err) {
    store.dispatch("PrepareMessage", {
      messageHeader: "",
      message: err.message,
      messageDetail: err,
      showMessage: true
    });
  }
}; //methods
