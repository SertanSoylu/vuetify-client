import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Unvan from './components/Unvan.vue'
import Brans from './components/Brans.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/unvan',
      name: 'unvan',
      component: Unvan
    },
    {
      path: '/brans',
      name: 'brans',
      component: Brans
    },
  ]
})
